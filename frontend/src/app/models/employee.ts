import {Department} from './department';

export class Employee {

  id: string;
  department: Department;
  name: string;
  age: number;
  position: string;

  constructor(o?: Partial<Employee>) {
    Object.assign(this, o);
  }
}
